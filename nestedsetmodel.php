<?php

// veritabanı bağlantı 
$dsn = 'mysql:host=localhost;dbname=roots';
$user = 'root';
$password = '';
$pdo = new PDO($dsn, $user, $password);

// Sadece ilk çalıştıgında çalışır ikinci açılışta db kurulu oldugundan hata verir
$query = "CREATE TABLE taxonomy_tree (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    lft INT,
    rgt INT
)";
$stmt = $pdo->prepare($query);
$stmt->execute();

// spotify taxonomy dosya bağlantı
$taxonomy_list = file('https://help.shopify.com/txt/product_taxonomy/en.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);


$query = "INSERT INTO taxonomy_tree (name, lft, rgt) VALUES ('Root', 1, 2)";
$stmt = $pdo->prepare($query);
$stmt->execute();

$lft = 2;
$rgt = 3;


foreach ($taxonomy_list as $name) {
    $query = "SELECT id, rgt FROM taxonomy_tree WHERE name = :name";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(':name', $name);
    $stmt->execute();
    $parent = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($parent) {
        $query = "UPDATE taxonomy_tree SET rgt = rgt + 2 WHERE rgt >= :rgt";
        $stmt = $pdo->prepare($query);
        $stmt->bindParam(':rgt', $parent['rgt']);
        $stmt->execute();
        $query = "UPDATE taxonomy_tree SET lft = lft + 2 WHERE lft > :rgt";
        $stmt = $pdo->prepare($query);
        $stmt->bindParam(':rgt', $parent['rgt']);
        $stmt->execute();
        $query = "INSERT INTO taxonomy_tree (name, lft, rgt) VALUES (:name, :rgt, :rgt + 1)";
        $stmt = $pdo->prepare($query);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':rgt', $parent['rgt']);
        $stmt->execute();
    } else {
        $query = "INSERT INTO taxonomy_tree (name, lft, rgt) VALUES (:name, :lft, :rgt)";
        $stmt = $pdo->prepare($query);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':lft', $lft);
        $stmt->bindParam(':rgt', $rgt);
        $stmt->execute();
        $lft += 2;
        $rgt += 2;
    }
}

$query = "SELECT name, lft, rgt FROM taxonomy_tree ORDER BY lft";
$stmt = $pdo->prepare($query);
$stmt->execute();
$taxonomy_tree = $stmt->fetchAll(PDO::FETCH_ASSOC);

function outputTaxonomyTree($taxonomy_tree, $parent_lft = 1, $parent_rgt = NULL) {
    foreach ($taxonomy_tree as $node) {
        if ($parent_rgt && $node['rgt'] > $parent_rgt) {
            break;
        }
        if ($node['lft'] > $parent_lft) {
            echo '<li>' . $node['name'];
            if ($node['lft'] + 1 != $node['rgt']) {
                echo '<ul>';
                outputTaxonomyTree($taxonomy_tree, $node['lft'], $node['rgt']);
                echo '</ul>';
                echo '</li>';
        }
    }
}
}
outputTaxonomyTree($taxonomy_tree);
